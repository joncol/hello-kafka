(ns hello-kafka.core
  (:require [kinsky.client :as client]
            [kinsky.async :as async]
            [clojure.core.async :as a :refer [go <! >!]])
  (:gen-class))

(defn -main
  [& args])

(let [p (client/producer {:bootstrap.servers "localhost:9092"}
                         (client/keyword-serializer)
                         (client/edn-serializer))]
  #_(client/send! p "Hello-Kafka" :account-a {:action :login})
  (client/send! p "my-replicated-topic" :something "hello Kafka, from Clojure again"))

(let [p (client/producer {:bootstrap.servers "localhost:9095"}
                         (client/keyword-serializer)
                         (client/edn-serializer))]
  #_(client/send! p "Hello-Kafka" :account-a {:action :login})
  (client/send! p "test" :something "hello Kafka, from Clojure"))
